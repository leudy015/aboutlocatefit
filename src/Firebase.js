import React from 'react'
import firebase from 'firebase'
import { connect } from 'react-firebase'

firebase.initializeApp({
  databaseURL: 'https://react-firebase-sandbox.firebaseio.com'
})

const Counter = ({ value, setValue }) => (
  <div>
    <button onClick={() => setValue(value - 1)}>-</button>
    <span>{value}</span>
    <button onClick={() => setValue(value + 1)}>+</button>
  </div>
)

export default connect((props, ref) => ({
  value: 'counterValue',
  setValue: value => ref('counterValue').set(value)
}))(Counter)
