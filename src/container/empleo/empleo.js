import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//component

import Header1 from "./../../component/header/header1.js";
import Footer from "./../../component/footer/footer.js";
import empleado from './../../assets/img/empleado.jpg';
import "./empleo.css"


class Empleo extends Component{
  render(){
    return(
      <div>

      <Header1 />

      <div className="descripcionempleo">


      <div className="block1">
          <h1>
          Ayúdanos a ser la comunidad de profesionales por excelencia.
          </h1>

          <p>En Locatefit queremos transformar el concepto de contratar profesionales.</p>
          <p>Cuando oyes Locatefit, seguramente piensas en una empresa que te ayuda a contratar profesionales. Suena bien, ¿no? Pero la verdadera historia es la que no se ve. Esta historia habla de un enorme crecimiento, grandes desafíos y una increíble oportunidad de futuro por delante. Y nos encantaría que formaras parte de ella.</p>
          <p>Queremos convertirnos en la comunidad de profesionales por excelencia, para que pueda contratar con total tranquilidad. </p>
          <p>Somos un equipo pequeño pero con una gran repercusión y queremos ser la solución a algunos de los dilemas más interesantes que plantea el mercado. Somos dinámicos y siempre estamos al acecho de ideas innovadoras. Echa un vistazo a lo que dicen algunos de nuestros equipos.</p>
      </div>

      <div className="blockimg">
      <img width={"80%"}  src={empleado} />

      </div>

    </div>

    <div className="applygeneral">

          <div className="applygeneraltittle">
            <h1>Aplica</h1>
            <p>Somos un grupo de emprendedores, apasionados por lo que hacemos, esperamos que encajes con nuestro equipo y así poder desarrollar un mejor servicio.</p>
          </div>

        <a href="mailto:info@locatefit.es" className="apply">  <div>

                <div className="applytittle">
                <h2>Backend Engineers Node Js GraphQL, Python</h2>
                </div>
                <div className="applydes">
                <p>¡1 Puesto disponible!</p>
                </div>
          </div></a>

          <a href="mailto:info@locatefit.es" className="apply"> <div>

                <div className="applytittle">
                <h2 >Analytics & Strategy. Numbers!</h2>
                </div>
                <div className="applydes">
                <p >¡1 Puesto disponible!</p>
                </div>
          </div> </a>


        <a href="mailto:info@locatefit.es" className="apply">  <div>

            <div className="applytittle">
            <h2>React Native and React Js Developer</h2>
            </div>
            <div className="applydes">
            <p>¡1 Puesto disponible!</p>
            </div>
          </div>
          <div>
          </div></a>
        <a href="mailto:info@locatefit.es"  className="apply">  <div>

              <div className="applytittle">
              <h2>Brand Partnership Marketing Manager </h2>
              </div>
              <div className="applydes">
              <p>¡1 Puesto disponible!</p>
              </div>
          </div></a>
      <a href="mailto:info@locatefit.es"  className="apply">  <div>

              <div className="applytittle">
              <h2>Graphic Designer and Animator</h2>
              </div>
              <div className="applydes">
              <p>¡1 Puesto disponible!</p>
              </div>
          </div></a>

        <a href="mailto:info@locatefit.es"  className="apply"> <div>

              <div className="applytittle">
              <h2>Amazon Web Service Expert</h2>
              </div>
              <div className="applydes">
              <p>¡1 Puesto disponible!</p>
              </div>

          </div>
          </a>



</div>

<div className="noen">
<p>¿No encuentras lo que quieres? Siempre estamos en busca de nuevos talentos, así que ponte en contacto con nuestro equipo de recursos humanos en <span className="mail">info@locatefit.es</span></p>

</div>


<Footer />








      </div>



    )
  }
}


export default Empleo;
