import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";


//component

import Headerprofesionales from "./../../component/header/headerprofesionales.js";
import Footer from "./../../component/footer/footer.js";
import empleoimg from './../../assets/img/empleo.svg';
import moneda from './../../assets/img/moneda.svg';
import calendar from './../../assets/img/calendar.svg';
import grupo from './../../assets/img/grupo.svg';
import "./empleo.css"


class Profesionales extends Component{
  render(){
    return(
      <div className="Profesionales">

          <Headerprofesionales />
          <div className="empeloimg">
                <h1> Gana hasta 1,345€/mes con un horario de trabajo flexible, hazte freelancer en menos de un  minuto.
                </h1>
                <a href="https://locatefit.es/register">Apuntarme</a>

          </div>

          <img className="empleoimg1"  src={empleoimg} />

          <div className="empleoimggeneral">

          <div className="empleoimg2">

          <img className="empleoimgl" height={100} src={moneda} />
          <p className="empleodes">Tu compensación</p>
          <p>Nuestros freelancers pueden ganar hasta 1,345€/mes.
          Lo que ganes por pedido dependerá de tu experiencia
          y las valoraciones obtenidas.</p>



          </div>

          <div className="empleoimg2">
          <img className="empleoimgl" height={100} src={calendar} />
          <p className="empleodes">Trabaja cuando quieras</p>
          <p>Trabaja como autónomo con total libertad.
          Elige dónde te conectas y que órdenes aceptas.</p>
          </div>

          <div className="empleoimg2">
          <img className="empleoimgl" height={100} src={grupo} />
          <p className="empleodes">Necesitarás…</p>
          <p>Una sonrisa de oreja a oreja, un iPhone o un
          dispositivo  Android, ser mayor de 18 años, y darse de alta como trabajador autónomo </p>
          </div>

          </div>

          <Footer />





      </div>



    )
  }
}


export default Profesionales;
