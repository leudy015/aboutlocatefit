import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";
import Headerpregunta from "./../../component/header/headerpregunta.js";
import Footer from "./../../component/footer/footer.js";
import "./ayuda.css"

class Preguntas extends Component {
  render() {
    return (
      <div className="Preguntas">
        <Headerpregunta />
        <div className="entry-content">
          <h3>Preguntas frecuentes</h3>
          <p>1.) ¿Cómo cancelo mi pedido?</p>
          <p>En caso de que quiera cancelar su pedido este debe hacerce antes que el vendedor acepte la orden en tu panel de control encontraras al opción de cancelar pedido.</p>

          <p>2.) ¿Cómo hago el seguimiento de mi pedido?</p>

          <p>Recibíras notificaciones de cada estado del pedido para que este enterado del progreso de tu orden</p>

          <p>3.) ¿Qué pasa si no estoy sastifecho con mi orden?</p>

          <p>Antes de valorar tambien tendra la obción de abrir una reclamación donde podrás explicarnos el problema con tu orden.</p>

          <p>4.) ¿Qué pasa con mi dinero en caso de reclamación?</p>

          <p>Estudiaremos tu caso y en un plazo de 48Horas tendrás tu dinero en cuenta.</p>
        </div>
        <Footer />
      </div>
    )
  }
}

export default Preguntas;
