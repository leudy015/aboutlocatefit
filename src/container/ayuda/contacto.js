import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//component

import Headercontacto from "./../../component/header/headercontacto.js";
import Footer from "./../../component/footer/footer.js";
import support from './../../assets/img/support.png';
import prensa from './../../assets/img/prensa.png';
import "./ayuda.css";



class Contacto extends Component{



  render(){
    return(

      <div className="Contacto">
          <Headercontacto />

        <div className="contactoscontent">


          <div className="Contactos">
                <img width="100px"  src={support} />
                <h3>Atención al Cliente</h3>
                <p>
                    ¿Alguna duda con tu pedido? ¡Para eso estamos! Mándanos un email a <a href="mailto:info@locatefit.es">info@locatefit.es</a> o llámanos al <a href="tel:?+34 689 35 15 92">+34 689 35 15 92</a>
                </p>
                <p>Por favor ten en cuenta que no aceptamos pedidos por teléfono, así que si necesitas hacer un pedido, hazlo online. El número de contacto es sólo para atención al cliente.</p>
            </div>



        <div className="Contactos">
                <img width="100px"  src={prensa} />
                <h3>Contacto de prensa</h3>
                <p>
                    Para entrevistas,  o cualquier tipo de solicitud relacionada con nuestro departamento de prensa, por favor escríbenos un correo a <a href="mailto:info@locatefit.es">info@locatefit.es</a>.
                </p>
                <p>Lo sentimos, pero nuestro departamento de prensa no tiene acceso a ese tipo de información. Para cualquier tipo de preguntas relacionadas con los pedidos, por favor contacta con atención al cliente en el número especificado más arriba.</p>
        </div>

        </div>

    <Footer />

      </div>



    )
  }
}


export default Contacto;
