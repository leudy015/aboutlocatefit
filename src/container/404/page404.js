import React from 'react'
import Header404 from "./../../component/header/header404.js";
import Footer from "./../../component/footer/footer.js";
import "./page404.css"
import depresion from './../../assets/img/depresion.svg'



const Notfound = () =>

 <div>
 <Header404 />
 <div className="noencontradosec">
 <img className="noencontradoimg" src={depresion} />

 <h3 className="noencontradodes">Parece que no podemos encontrar lo que estás buscando.</h3>

 </div>

 <Footer />

 </div>


export default Notfound;
