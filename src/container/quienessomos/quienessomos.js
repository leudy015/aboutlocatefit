import React, { Component } from 'react';
import Header from "./../../component/header/header.js";
import Footer from "./../../component/footer/footer.js";
import quienes from './../../assets/img/quienes.jpg';
import lupa from './../../assets/img/lupa.svg';
import home from './../../assets/img/home.svg';
import movil from './../../assets/img/movil.svg';
import person from './../../assets/img/person.svg';
import leudy from './../../assets/img/leudy.jpg';
import "./quienes.css";

class Quienes extends Component {
  render() {
    return (
      <div className="Quienes">
        <Header />
        <div className="Quienes1">
          <h1>Los mejores profesionales, tu eliges la fecha, hora y lugar.</h1>
          <p> Locatefit es la plataforma líder para conectar a las personas que buscan cualquier tipo de servicios con profesionales independientes de alta calidad y previamente seleccionados. Desde la limpieza del hogar hasta cuidado personal, Locatefit iguala instantáneamente a miles de clientes cada semana con los mejores profesionales en ciudades de todo el mundo. Con un proceso continuo de reserva de 60 segundos, pago seguro y respaldado por la Garantía de Locatefit S.L., Locatefit es la forma más fácil y conveniente de reservar servicios a domicilio.</p>
        </div>
        <img className="quienes" src={quienes} />
        <div>
          <div className="Quienes4">
            <h1>Una necesidad, una simple idea que se convirtió en una comunidad de 12 millones</h1>
          </div>
          <div className="Quienes22">
            <div className="Quienes222">
              <div className="Quienes5">
                <img width='70px' src={lupa} />
                <p>Somos una herramienta que te ayuda a prestar tus servicios profeisonales a personas cerca de ti, un ingreso extra, tu decides cuándo y cómo ofrecer tus servicios, empieza ahora y se libre.</p>
              </div>
              <div className="Quienes5">
                <img width='70px' src={person} />
                <p>Contamos con profesionales exelentes y dedicados a brindarte un mejor servicio y más ecómonico, en nuestra herramienta, las valoraciones de nuestros clientes es fundamental, por eso somos los mejores.</p>
              </div>
              <div className="Quienes5">
                <img width='70px' src={movil} />
                <p>Encontrar personas cerca de ti para que te ayuden con tus tareas es nuestro objetivo y de la forma más ecónomica, somos un comunidad en crecimiento, únete a la bola de nieve y disfruta los beneficios.</p>
              </div>
              <div className="Quienes5">
                <img width='70px' src={home} />
                <p>Todos nuestros servicios se realizan a domicilio para mayor facilidad para nuestros clientes, nuestros profesionales son de confianza, mira sus valoraciones, así que, con confianza, ábrele la puerta de tu casa.</p>
              </div>
            </div>
          </div>
          <div className="Quienes_11">
            <h1>El equipo directivo</h1>
          </div>
          <div className="Quienes_1">
            <img width="300px" src={leudy} />
            <h4>Leudy Leonardo Martes Concepcion</h4>
            <p>CEO Funder Locatefit</p>
            <p>Nuestra misión es que las personas profesionales puedan brindar sus servicios a miles de personas de su entorno con la finalidad de que pueda tener mayores ingresos y mejor calidad de vida, te invitamos a que pruebes nuestra herramienta y sé libre con mayoes ingresos.</p>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}
export default Quienes;
