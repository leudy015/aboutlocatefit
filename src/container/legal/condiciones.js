import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";
import Headercondiciones from "./../../component/header/headercondiciones.js";
import Footer from "./../../component/footer/footer.js";
import "./legal.css"

class Condiciones extends Component {
  render() {
    return (
      <div className="Condiciones">
        <Headercondiciones />
        <div className="entry-content">
          <h3 style={{marginBottom: 50}}className="entry-content1" >Gracias por usar Locatefit.es Esperamos que lo encuentres divertido y práctico. Para el buen funcionamiento de la comunidad, necesitamos que sigas las reglas y términos y condiciones propuestas.</h3>

          <h3>Términos y Condiciones de Locatefit S.L.</h3>

          <p>A continuación se establecen las “Condiciones Generales” que regulan el uso de los servicios de Locatefit, tanto en su versión web, como a través de la aplicación móvil. Estas Condiciones Generales constituyen un acuerdo vinculante entre cualquier usuario y Locatefit S.L., y se considerarán aceptados por su parte cada vez que usted acceda a cualquier al portal web o a la aplicación móvil. Por lo que, cada usuario debe leer atentamente estas Condiciones Generales y si no las entiende, o no las acepta, no podrá utilizar los servicios.

            Al aceptar estas Condiciones Generales usted manifiesta que es mayor de edad, que tiene plena capacidad de obrar, que ha leído y comprendido las Condiciones de uso que a continuación se desarrollan y demás términos legales como la <a href='https://about.locatefit.es/privacidad'>política de privacidad</a> o la <a href='https://about.locatefit.es/cookies'>política de cookies:</a></p>

          <h3>1. Titularidad del servicio</h3>

          <p>Tanto las plataformas móviles publicadas, como el presente portal web, son propiedad de Locatefit, S.L., sociedad española, domiciliada en Burgos (09005-Burgos), Calle Francisco Sarmiento, número 13 con C.I.F. B-09603440 e inscrita en el Registro Mercantil de Burgos, al Tomo 00000, Folio 000, Sección 0, Hoja 0000000 (en adelante “Locatefit”).</p>

          <p>Cualquier usuario podrá contactar con la empresa bien en su domicilio social, bien a través de nuestro correo electrónico <a href='mailto:legal@locatefit.es'>legal@locatefit.es</a> o en el teléfono que aparece en el portal web.</p>

          <h3>2. Condiciones de uso de los servicios</h3>

          <p>Los servicios se podrán prestar tanto desde la Plataforma Móvil como desde la página web, pero en su mayoría se desarrollarán a través de la Plataforma Móvil, actuando como el lugar para que (i) los clientes puedan contratar profesionales. (en adelante “El Cliente”) y para que (ii) los prestadores de servicios accedan a las contrataciones de los clientes(en adelante “El profesional”). Aunque a lo largo de los presente términos nos referiremos a ambos roles de forma conjunta como “Usuarios”.</p>

          <p>Por tanto, la finalidad de Locatefit es proporcionar un lugar para que los individuos creen contactos con fines de prestar o demandar servicios. Lo que implica que la relación contractual será entre el Cliente y el Profesional. Locatefit no asume ninguna obligación de dicha relación contractual puesto que simplemente su función es poner en contacto a ambas partes para que negocien tanto el servicio a prestar como el precio a abonar. Por tanto, Locatefit no está involucrado en la transacción, ni recomienda Profesionales. El Cliente reconoce expresamente que es de su exclusiva responsabilidad seleccionar un Profesional de servicios y negociar con él los términos relativos a la realización de cualquier servicio o colaboración. Locatefit no garantiza que un Profesional sea el más apropiado para realizar el servicio solicitado por el Cliente. Locatefit no da garantías sobre la calidad o la naturaleza de los servicios y/o colaboraciones realizados, ni cualquier otra garantía. Cualquier compromiso, declaración o garantía en este sentido serán facilitadas únicamente por el Profesional bajo los términos acordados con el Cliente.</p>

          <p>Por ello, la información contenida en la Plataforma Móvil y el portal web es facilitada por los Clientes y los Profesionales, por lo que Locatefit no tiene control sobre la exactitud, fiabilidad, la veracidad de las ofertas publicadas o de los Contenidos generados ni por los Clientes ni por los Profesionales.</p>

          <p>Por lo expuesto, Locatefit no se hace responsable de los Contenidos de los diferentes roles de usuario, de la calidad, seguridad o legalidad de los servicios o datos profesionales publicados, de la veracidad o precisión de los listados de profesionales, la capacidad de los clientes de servicios para ofrecer oportunidades de colaboración o prestación de servicios a los profesionales.</p>

          <p>En cambio, si Locatefit detecta que cualquier contenido, o perfil de usuario, ya sea Cliente o Profesional, contraviene las presentes condiciones de uso, o realiza cualquier acto ilegal, borrará o modificará aquellos contenidos o perfiles en un plazo razonable desde que tenga conocimiento de ello.</p>

          <p>No obstante, y hechas las salvedades anteriores, se advierte que el Contenido de Locatefit generado por cualquier usuario puede contener inexactitudes o errores tipográficos. Por lo que Locatefit no garantiza la exactitud, fiabilidad, y veracidad de dichos Contenidos.</p>

          <p>Cada Usuario hará uso de la Plataforma Móvil y del portal web, dependiendo del rol que desempeñe, bien siendo Cliente, bien siendo Profesional bajo su cuenta y riesgo. Por lo que Locatefit no puede garantizar ni promete resultados específicos al usar la Plataforma Móvil ni el portal web.</p>

          <h3>3. Obligaciones para todos los usuarios de Locatefit</h3>

          <p>Los usuarios que utilicen la Plataforma Móvil y el portal web aceptan las presentes Condiciones Generales de modo que se asegure el cumplimiento de todas las leyes y normativas aplicables. En particular, tanto los Clientes como los Profesionales no usarán la Plataforma Móvil ni el portal web para exhibir, transmitir, distribuir o vincular cualquier material que (i) Sea falso o engañoso. (ii) Sea discriminatorio, difamatorio, ofensivo u obsceno o que pueda incitar a discriminación, odio o violencia contra una persona o grupo de personas a causa de su origen o su pertenencia a un grupo étnico, país, raza o religión específica, o a causa de su género, orientación sexual o incapacidad. (iii) Afecte a la intimidad o el orden público. (iv) o que, de cualquier forma, resulte ilícito, infrinja la legalidad vigente o perjudique a terceros.</p>

          <p>Por otra parte, los usuarios se comprometen a no usar ni intentar usar, y hará que cada parte bajo su control no use ni intente usar ningún motor, software, herramienta, agente u otro sistema o mecanismo (incluidos, sin limitación, navegadores, ‘spiders’, ‘avatars’, o agentes inteligentes) para navegar o hacer búsquedas en el Sitio Web o Plataforma Móvil, excepto el motor de búsqueda y los agentes de búsqueda que están disponibles en el Sitio Web. Los usuarios se comprometen a no usar el Sitio Web, la Plataforma Móvil o los servicios puestos a su disposición de forma que el Sitio Web, la Plataforma Móvil o dichos servicios sean interrumpidos, perjudicados, minorar su eficiencia o menoscabados de cualquier forma.</p>

          <p>Asimismo, se comprometen a asumir la responsabilidad total por el uso del Sitio Web y la Plataforma Móvil hecho por el usuario (incluyendo sus subcontratistas, agentes, colaboradores, asociados, dependientes o, en cualquier otra forma, tengan relación con él) y, consecuentemente, acepta la responsabilidad conforme a las presentes Condiciones Generales respecto al uso del Sitio Web o la Plataforma Móvil por terceros que sean autorizados por el usuario y que incumplan dichas Condiciones Generales.</p>

          <p>Los usuarios se compromete a no usar el Sitio Web o Plataforma Móvil para obtener acceso o tratar de acceder sin autorización a otros sistemas informáticos.</p>

          <h3>4. Obligaciones para todos los usuarios considerados Clientes</h3>

          <p>De forma particular los Clientes se comprometen a (i) contratar o demandar servicios reales y no inventados; (ii) revisar la veracidad de todas las informaciones o documentos presentados por el Profesional antes de aceptar cualquiera de los servicios ofertados; (iii) abonar los honorarios propuestos por los profesionales.</p>

          <h3>5. Obligaciones para todos los usuarios considerados Profesionales</h3>

          <p>Por su parte los Profesionales se comprometen a (i) cumplir con todas las obligaciones legales para el desarrollo de su actividad; (ii) prestar los servicios de forma diligente y con las máximas garantías; (iii) prestar el servicio conforme a las especificaciones marcadas por el Cliente; (iv) ofrecer las garantías legales oportunas al Cliente por los servicios prestados.</p>

          <h3>6. Servicios de suscripción</h3>

          <p>Locatefit ofrece a los Profesionales diferentes modalidades de gestión y pago, pudiendo éstos acceder a las ofertas de trabajo bien mediante recarga de saldo bien a través de una suscripción. Todos los planes incluirán el precio correspondiente e impuestos que sean de aplicación en cada caso en base al acuerdo alcanzado con Locatefit. Se podrá contratar la modalidad y tarifa que se desee a través del Sitio Web, Plataforma Móvil o telefónicamente. El Servicio se entenderá contratado por el Profesional en el momento en el que se envíe a Locatefit la orden de compra a través del Sitio Web o la Plataforma Móvil habilitada a tal efecto. Cuando la contratación se realice de manera telefónica, el Profesional recibirá un email confirmando la modalidad contratada.</p>

          <h3>7.Código de conducta</h3>

          <p>Desde Locatefit existen varios mecanismos para la interactuación, tanto con los Clientes como con los Profesionales, así como cualquier otro internauta que solicite información. Cualquier de ellos, podrá utilizar, ya sea el formulario de contacto, como el chat de atención del cliente, así como la redes sociales de la empresa, siempre y cuando se cumplan con el siguiente código de conducta:</p>

          <ul>
            <li>
              1- NINGÚN USUARIO PODRÁ HACERSE PASAR POR OTRA PERSONA U ORGANIZACIÓN.
            </li>
            <li>
              2- NO SE PODRÁ UTILIZAR UN LENGUAJE IRRESPETUOSO Y OFENSIVO.
            </li>
            <li>
              3- QUEDA TOTALMENTE PROHIBIDO presentar, citar y recomendar empresas, o portales web, y en general realizar acciones de marketing o spam. Ese tipo de contenidos serán eliminados junto con las cuentas que llevaron a cabo tal acción e incluso ponerlo en conocimiento de la Agencia Española de Protección de Datos.
            </li>
          </ul>

          <h3>8. Contenido y presentaciones de información del usuario</h3>

          <p>Para el correcto funcionamiento del portal web y de la Plataforma Móvil es necesario la recopilación de datos personales, por ello, todos los datos facilitados bien sea, con el mero envío de un formulario de contacto, la interactuación con el chat de atención al cliente, instalarse la Plataforma Móvil, o registrarse en el portal web serán incorporados a los ficheros y tratamientos de datos cuya titularidad ostenta Locatefit, SL para la consecución de las siguientes finalidades:</p>

          <ul>
            <li>
              Responder a las solicitudes de información recibidas a través de los formularios de contacto
            </li>
            <li>
              Gestionar el registro como usuario
            </li>
            <li>
              Enviarle informaciones comerciales sobre nuevos servicios y promociones de la empresa
            </li>
          </ul>

          <p>Así mismo, cualquier persona antes de facilitar información a Locatefit, debe leer y aceptar los términos descritos en la <a href='https://about.locatefit.es/privacidad'>política de privacidad</a>.</p>

          <h3>9. Utilización de cookies y tecnologías similares</h3>

          <p>Por otra parte, Locatefit le informa que por visitar su portal web no queda registrado de forma automática ningún dato de carácter personal que identifique a un Usuario, en cambio existe determinada información de carácter no personal y no identificable con un Usuario concreto que se recoge durante la sesión en directo para a través de dispositivos denominados “cookies” que nos permiten obtener información estadística sobre el uso del portal web para luego poder realizar mejoras. Todo usuario debe consultar nuestra  <a href='https://about.locatefit.es/cookies'>política de cookies</a>para navegar por la web.</p>

          <h3>10. Propiedad Intelectual e Industrial</h3>

          <p>Todos los contenidos del Sitio Web, tales como textos, gráficos, fotografías, logotipos, iconos, imágenes, así como el diseño gráfico, código fuente y software, son de la exclusiva propiedad de Locatefit o de terceros, cuyos derechos al respecto ostenta legítimamente Locatefit,estando por lo tanto protegidos por la legislación nacional e internacional. Por lo queda estrictamente prohibido la utilización de todos los elementos objeto de propiedad industrial e intelectual con fines comerciales así como su distribución, modificación, alteración o descompilación, con especial atención a todos los diseños de las colecciones mostradas.</p>

          <p>Así mismo, Locatefit al poner a disposición de todos los usuarios su Plataforma Móvil concede a cada uno de ellos el derecho no exclusivo e intransferible de utilizar el Software, teniendo en cuenta las siguientes limitaciones:</p>
          <ul>
            <li>
              El Usuario sólo podrá utilizar el Software para las funcionalidades propias descritas en el presente documento.
            </li>
            <li>
              No realizará ninguna copia del Software sin la autorización pertinente de Locatefit, SL...
            </li>
            <li>
              Queda prohibida la ingeniería inversa. El Usuario no podrá aplicar técnicas de ingeniería inversa, descompilar o desensamblar el Software, ni realizar cualquier otra operación que tienda a descubrir el código fuente.
            </li>
            <li>
              Queda prohibido el arrendamiento. El Usuario no arrendará ni alquilará el Software a otra persona.
            </li>
            <li>
              Queda prohibido crear ninguna alteración, adaptación, modificación, traducción, mejora ni trabajo derivado a partir del Software
            </li>
            <li>
              Queda prohibido realizar cualquier acto que pueda ser considerado una vulneración de cualesquiera derechos de propiedad intelectual o industrial pertenecientes a Locatefit, SL.
            </li>
          </ul>

          <h3>11. Exclusión de garantías y responsabilidades</h3>

          <p>En referencia a las relaciones creadas entre los Clientes y Profesionales, Locatefit no asume ninguna responsabilidad en relación con cualquier contrato u otro acuerdo celebrado entre el Cliente y el Profesional, asimismo, sin limitación alguna Locatefit se exime y declina toda responsabilidad en relación con la calidad o adecuación de cualquier tarea realizada, o no realizada, por un Profesional. En consecuencia,Locatefit no es responsable ante el Cliente ni ante el Profesional en cualquier situación de conflicto, acto u omisión resultante de la relación contractual entre ellos, incluyendo cualquier daño padecido, sean, a título enunciativo y no limitativo, daños directos, indirectos, emergente,lucro cesante, y los que tengan su origen, causa y/o consecuencia en la relación.</p>

          <p>Locatefit no asume ninguna responsabilidad por la exactitud de los datos o documentos proporcionados por el Profesional, así como por daños a una persona, propiedad u otros, resultantes de la presentación de una propuesta fraudulenta. El Cliente acepta todos los riesgos asociados con el trato con extraños o personas que actúen bajo apariencias falsas, así como todos los riesgos asociados con el trato con otros usuarios con los que pueda entrar en contacto a través de la Plataforma Móvil y el portal web, siendo su exclusiva responsabilidad adoptar las medidas de seguridad pertinentes.</p>

          <p>Respecto al propio funcionamiento de la Plataforma Móvil y el portal web, Locatefit se reserva el derecho a interrumpir el acceso a los servicios en cualquier momento y sin previo aviso, ya sea por motivos técnicos, de seguridad, de control, de mantenimiento, por fallos de suministro eléctrico o por cualquier otra causa justificada.</p>

          <p>En consecuencia, Locatefit no garantiza la fiabilidad, la disponibilidad ni la continuidad de la Web ni de los Servicios, por lo que la utilización de los mismos por parte de los usuarios se lleva a cabo por su propia cuenta y riesgo, sin que, en ningún momento, puedan exigirse responsabilidades a Locatefit en este sentido.</p>

          <p>Además Locatefit no asume responsabilidad alguna derivada, a título enunciativo pero no limitativo:</p>
          <ul>
            <li>
              De la utilización que los usuarios hagan de los materiales dispuestos en la web, ya sean prohibidos o permitidos, en infracción de los derechos de propiedad intelectual y/o industrial de contenidos de la propia web o de los portales de terceros.
            </li>

            <li>
              De los eventuales daños y perjuicios a los usuarios causados por un funcionamiento normal o anormal de las herramientas de búsqueda, de la organización o la localización de los contenidos y/o acceso a los servicios y, en general, de los errores o problemas que se generen en el desarrollo o instrumentación de los elementos técnicos que forman el servicio.
            </li>

            <li>
              De los contenidos de aquellas páginas a las que los usuarios puedan acceder desde enlaces incluidos en la web.
            </li>

            <li>
              De los actos u omisiones de terceros, con independencia de que estos terceros pudiesen estar unidos a Locatefit mediante vía contractual.
            </li>

            <li>
              De igual modo, Locatefit excluye cualquier responsabilidad por los daños y perjuicios de toda clase que puedan deberse a la presencia de virus o a la presencia de otros elementos lesivos en los contenidos que puedan producir alteración en los sistemas informáticos así como en los documentos o sistemas almacenados en los mismos, por lo que Locatefit no será responsable en ningún caso cuando se produzcan:
            </li>

            <li>
              Errores o retrasos en el acceso a los servicios por parte del usuario a la hora de introducir sus datos en el formulario correspondiente o cualquier anomalía que pueda surgir cuando estas incidencias sean debidas a problemas en la red Internet, causas de caso fortuito o fuerza mayor y cualquier otra contingencia imprevisible ajena a la buena fe de Locatefit.
            </li>

            <li>
              Fallos o incidencias que pudieran producirse en las comunicaciones, borrado o transmisiones incompletas, de manera que no se garantiza que los servicios del sitio web estén constantemente operativos.
            </li>

            <li>
              De los errores o daños producidos al sitio web por un uso del servicio ineficiente y de mala fe por parte del usuario.
            </li>

            <li>
              De la no operatividad o problemas en la dirección de email facilitada por el usuario para el envío de la información solicitada.
            </li>
          </ul>

          <p>En todo caso, Locatefit se compromete a solucionar los problemas que puedan surgir y a ofrecer todo el apoyo necesario al usuario para llegar a una solución rápida y satisfactoria de la incidencia.</p>

          <h3>12. Enlaces a otros sitios web</h3>

          <p>Locatefit no garantiza ni asume ningún tipo de responsabilidad por los daños y perjuicios sufridos por el acceso al Servicios/contenido de terceros a través de conexiones, vínculos o links de los sitios enlazados ni sobre la exactitud o fiabilidad de los mismos. La función de los enlaces que aparecen en Locatefit es exclusivamente la de informar al usuario sobre la existencia de otras fuentes de información en Internet, donde podrá ampliar los Servicios ofrecidos por el Portal. Locatefit no será en ningún caso responsable del resultado obtenido a través de dichos enlaces o de las consecuencias que se deriven del acceso por los usuarios a los mismos. Estos Servicios de terceros son proporcionados por éstos, por lo que Locatefit no puede controlar y no controla la licitud de los Servicios ni su calidad. En consecuencia, el usuario debe extremar la prudencia en la valoración y utilización de la información y servicios existentes en los contenidos de terceros.</p>

          <p>Queda expresamente prohibida la introducción de hiperenlaces con fines mercantiles en páginas web ajenas a Locatefit que permitan el acceso al presente portal web sin consentimiento expreso de Locatefit. En todo caso, la existencia de hiperenlaces en sitios web ajenos a la empresa, no implicará en ningún caso la existencia de relaciones comerciales o mercantiles con el titular de la página web donde se establezca el hiperenlace, ni la aceptación por parte de Locatefit.</p>

          <h3>13. Duración y terminación</h3>

          <p>Estas Condiciones Generales permanecerán en vigor mientras sea usuario de la Plataforma Móvil. Locatefit se reserva el derecho, bajo su criterio exclusivo, para emplear todos los recursos legales incluyendo, de forma enunciativa pero no limitativa, la retirada del Contenido, la terminación inmediata de su registro y la capacidad de acceder a la Plataforma Móvil y/o cualquier otro servicio que le proporcione Locatefit, debido a cualquier incumplimiento que se realice de estas Condiciones Generales o del resto de los documentos legales publicados por la empresa.</p>

          <h3>14. Actualizaciones</h3>

          <p>Locatefit puede revisar estas Condiciones Generales en cualquier momento, actualizando esta página. En el caso de una nueva actualización, Locatefit le notificará el cambio de condiciones en su cuenta personal mediante el correo electrónico suministrado.</p>

          <h3>16. General</h3>

          <p>Estas Condiciones Generales están regidas por la normativa española, con renuncia expresa a cualquier otra. Si cualquier tribunal con jurisdicción competente acordara la nulidad de cualquier disposición de estas Condiciones Generales, la nulidad de dicha disposición no afectará a la validez del resto de las disposiciones de estas Condiciones Generales, que permanecerán en vigor. Ninguna renuncia de cualquier término de estas Condiciones Generales será interpretada como una renuncia posterior o continuada de dicho término o de cualquier otro término. Además, el hecho de que Locatefit no exija el cumplimiento de cualquier término de estas Condiciones Generales no se interpretará como una renuncia de dichas Condiciones ni afectará ni limitará de cualquier otra forma la capacidad de Locatefit de hacer cumplir y de exigir el cumplimiento de dichas Condiciones Generales en cualquier momento en el futuro.</p>

          <h3>16. Jurisdicción aplicable</h3>

          <p>Para cuantas cuestiones interpretativas o litigiosas que no tengan que ver con el proceso de contratación entre el Cliente y el Profesional, que pudieran plantearse sobre el portal web y la Plataforma Móvil será de aplicación la legislación española y en caso de controversia, ambas partes acuerdan someterse, con renuncia a cualquier otro fuero que pudiera corresponderle, a la jurisdicción de los Juzgados y Tribunales de la ciudad de Burgos (España).</p>
        </div>
        <Footer />
      </div>
    )
  }
}


export default Condiciones;
