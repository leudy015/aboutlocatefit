import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";
import Headerlegal from "./../../component/header/headerlegal.js";
import Footer from "./../../component/footer/footer.js";
import "./legal.css"

class Privacidad extends Component {
  render() {
    return (
      <div className="Privacidad">
        <Headerlegal />
        <div class="entry-content">
          <h3>PROTECCIÓN DE DATOS</h3>

          <p>Razón Social: Locatefit S.L.</p>
          <p>CIF: B09603440</p>
          <p>Domicilio: Calle Franccisco Sarmiento 13, 8A, 09005, Burgos</p>
          <p>Teléfono: +34 689 35 15 92</p>
          <p>Correo electrónico: info@locatefit.es</p>

          <h3>1. Política de Privacidad y Seguridad</h3>
          <p>La siguiente información sobre nuestra Política de Privacidad refleja el compromiso de Locatefit, SL (en adelante Locatefit) por mantener y garantizar relaciones comerciales seguras mediante la protección de los datos personales, garantizando el derecho a la privacidad de cada uno de los usuarios de nuestros servicios. En este documento explicamos cómo utilizamos los datos personales de los usuarios del portal web y de la Plataforma Móvil.</p>

          <h3>2. Definición de dato personal</h3>

          <p>Como “Dato Personal” debemos entender cualquier información concerniente a una persona física identificada o identificable. Entre otros, se incluyen el nombre, apellidos, la dirección postal y electrónica, así como el número de teléfono.</p>

          <h3>3. Tratamientos y finalidades de los datos recogidos</h3>

          <p>Cualquier dato personal que nos facilite al visitar cualquiera de nuestros portales web o con la instalación de nuestra aplicación móvil, será tratado de conformidad con las normas de protección de datos y sólo serán recogidos, tratados y utilizados para fines lícitos, legítimos e informados. Por ello, detallamos todas las finalidades con las que utiliza datos personales Locatefit como responsable del tratamiento:</p>

          <ul>
          <li>
          Gestionar los formularios de registro para poder convertirse en usuarios registrados y acceder a los servicios, además para contactarles y responderles sobre las cuestiones planteadas y recibir información comercial sobre nuevos productos y novedades de la empresa.
          </li>
          <li>
          Otra finalidad para que se recopilan datos es para responder a las consultas mediante el envío de formularios de contacto o chat de atención al cliente. Los datos obtenidos a través de esta vía son para responder a los propios usuarios y remitirle información comercial, además de incluirlos en la lista de distribución.
          </li>
          </ul>

          <p>Desde Locatefit informaremos a todos los usuarios el carácter no obligatorio de la recogida de determinados datos de carácter personal, salvo en los campos que se indique lo contrario. No obstante, la no cumplimentación de dichos datos podrá impedir a Locatefit prestar todos aquellos Servicios vinculados a tales datos, liberándonos de toda responsabilidad por la no prestación o prestación incompleta de estos Servicios.</p>

          <p>Corresponde al usuario la obligación de facilitar los datos de manera veraz y mantenerlos actualizados, por lo que Locatefit se reserva el derecho de excluir de los servicios y proceder a la cancelación del servicio a todo usuario que haya facilitado datos falsos, sin perjuicio de las demás acciones que procedan en Derecho.</p>

          <p>Para la consecución de estas finalidades, la Plataforma Móvil requiere de la autorización de determinados permisos durante su instalación. A continuación, procedemos a informar y detallar los permisos necesarios para el funcionamiento de nuestra aplicación, así como las finalidades subsidiarias con las que trataremos los datos facilitados:</p>

          <h4>Localización:</h4>

            <ul>
            <li>
            Locatefit: Ayudar a autocompletar la localización de la contratación.
            </li>
            </ul>

        <h4>Contactos:</h4>
              <ul>
              <li>
              Almacenar de manera automática el contacto de Atención al cliente cuando el usuario o el profesional lo solicitan.
              </li>
              </ul>

        <h4>SMS:</h4>
              <ul>
              <li>
              Recibir el SMS con el código de activación de la cuenta.
              </li>
              </ul>

        <h4>Teléfono:</h4>
              <ul>
              <li>
              Permiso para que el usuario (o profesional) puedan realizar llamadas desde la app.
              </li>
              </ul>

          <h4>Fotos:</h4>
              <ul>
              <li>
              Permiso para acceder a la biblioteca de imágenes o la cámara y que el usuario (o el profesional) puedan escoger una imagen de perfil, enviarla a través del chat de la aplicación o adjuntarla a una solicitud o publicar un servicio.
              </li>
              </ul>

          <h4>Almacenamiento:</h4>
              <ul>
              <li>
              Guardar archivos internos como sesiones de usuario, tokens y caché.
              </li>
              </ul>

              <p>También informamos que actualmente en las aplicaciones utilizamos las siguientes librerías con el objetivo de traquear movimientos de usuarios y campañas de marketing:</p>

              <ul>
              <li>
              Facebook SDK
              </li>

              <li>
              Adjust
              </li>

              <li>
              Firebase
              </li>

              <li>
              Mixpanel
              </li>

              <li>
              OneSignal
              </li>
              </ul>

              <p>En consecuencia, la unión de estos permisos, más la información facilitada por el usuario, nos permitirá:</p>

              <ul>
              <li>
              Utilizar toda la información con carácter comercial para la personalización de los servicios y realizar tratamientos estadísticos para realizar actividades de investigación y comercialización de los servicios y productos de Locatefit así como elaboración de perfiles e informes estadísticos relacionados con la actividad del usuario durante la navegación en el sitio web y/o la aplicación o su participación en acciones comerciales, publicitarias o promocionales.
              </li>

              <li>
              Desarrollar y mostrar publicidad adaptada a sus preferencias personales. Lo que podrá suponer el envío de comunicaciones comerciales, publicitarias o promocionales de productos y servicios de Locatefit que estuvieran relacionados con los servicios solicitados, así como de actualizaciones de los contenidos del sitio web y/o la aplicación.
              </li>

              <li>
              Verificar la inexistencia de actuaciones fraudulentas o contrarias a las condiciones de uso del sitio web y la aplicación, así como las Políticas de Privacidad y Cookies.
              </li>

              <li>
              Identificar y verificar que el usuario cumple con los requisitos necesarios para poder utilizar el sitio web y/o la aplicación y realizar sus acciones de forma lícita.
              </li>

              <li>
              Eliminar justificadamente a cualquier usuario que incumpla las condiciones de uso del sitio web y/o la aplicación.
              </li>
              </ul>

          <h3>4. Legitimación para el tratamiento de sus datos</h3>

          <p>La base legal para el tratamiento de sus datos es la respuesta a las consultas planteadas o llevar a cabo el registro de usuario a través de la página web o la Plataforma Móvil.</p>

          <h3>5. Plazo de conservación de los datos</h3>

          <p>Los datos serán conservados el tiempo necesario, para dar respuesta a las consultas o para la gestión integral del registro como bien como Cliente bien como Profesional hasta que cada titular de los datos manifiesta su voluntad de que cese el tratamiento de sus datos o para cumplir con el plazo legal previsto para el cumplimiento de determinadas obligaciones fiscales.</p>

          <h3>6. Comunicación de sus datos personales a terceros</h3>

          <p>Por el mero hecho de realizar una consulta a través de nuestros formularios sus datos personales NO serán cedidos ni comunicados a ningún tercero.</p>

          <p>En cambio, para la correcta gestión de la finalidad de Locatefit, los datos de los Clientes serán compartidos con los Profesionales para que éstos puedan proceder a presupuestar los servicios solicitados. Fuera de este supuesto no se van a comunicar los datos a ningún tercero. En el caso de que fuera necesario comunicar su información a algún otro tercero se solicitaría su consentimiento.</p>

          <h3>7. Seguridad de sus datos personales</h3>

          <p>Locatefit tiene una preocupación especial por garantizar la seguridad de sus datos personales. Sus datos son almacenados en nuestros sistemas de información, donde hemos adoptado e implantado medidas de seguridad, técnicas y organizativas, para prevenir cualquier pérdida o uso no autorizado por terceros, por ejemplo nuestros portales web utilizan protocolos de cifrado de datos Https.</p>

          <h3>8. Información sobre la Utilización de cookies</h3>

          <p>Por el mero hecho de visitar el presente portal web o utilizar los servicios de Locatefit no queda registrado de forma automática ningún dato de carácter personal que identifique a un Usuario. Sin embargo, le informamos que durante la navegación por el Sitio Web se utilizan “cookies”, pequeños ficheros de datos que se generan en el ordenador del internauta y que nos permiten obtener la siguiente información analítica: </p>

          <p>a) La fecha y hora de acceso a la Web, permitiendo saber las horas de más afluencia, y hacer los ajustes precisos para evitar problemas de saturación en nuestras horas punta.</p>

          <p>b) El número de visitantes diarios de cada sección, permitiendo conocer las áreas de más éxito y aumentar y mejorar su contenido, con el fin de que los usuarios obtengan un resultado más satisfactorio y mejorar el diseño de los contenidos.</p>

          <p>c) La fecha y hora de la última vez que el usuario visitó el Sitio Web para realizar estudios analíticos y estadísticos sobre el uso que tiene la web.</p>

          <p>d) Elementos de seguridad que intervienen en el control de acceso a las áreas restringidas.</p>

          <p>Las cookies son identificadores alfanuméricos que transferimos al disco duro de tu ordenador a través de tu navegador para permitir a nuestros sistemas reconocer tu navegador y decirnos cómo y cuándo las páginas de nuestro sitio son visitadas y por cuánta gente. Las cookies de Locatefit no recopilan información personal, y nosotros no combinamos la información general recogida a través de cookies con otra información personal para saber quién eres o cuál es tu dirección de email.</p>

          <p>La mayoría de los navegadores tienen una opción para desactivar la función de cookies, que evita que tu navegador acepte nuevas cookies, así como permitirte decidir si deseas aceptar cada nueva cookie en una variedad de formas. Para obtener más información sobre las cookies y cómo rechazarlas.</p>

          <p>Cualquier usuario debe leer y aceptar nuestra Política de Cookies antes de navegar por el presente portal web.</p>

          <h3>9. Derechos de los usuarios</h3>

          <p>Todos los usuarios pueden ejercitar cualquier de los derechos otorgados por la normativa de protección de datos, como el derecho de acceso, limitación del tratamiento, supresión, portabilidad, etc. mediante escrito dirigido a Locatefit o enviando un correo electrónico a <a href='mailto:legal@locatefit.es'>legal@locatefit.es</a></p>

          <p>En el caso de que cualquier usuario no se encuentre satisfecho con la solución aportada por la empresa, podrá acudir y tramitar la pertinente reclamación ante la Agencia Española de Protección de Datos.</p>

          <h3>10. ¿No desea recibir información de nosotros o desea revocar su consentimiento?</h3>

          <p>Cualquier usuario puede oponerse al uso de su información para fines publicitarios, investigaciones de mercado o desarrollo de encuestas de satisfacción, así como revocar su consentimiento en cualquier momento (sin efecto retroactivo). Para ello, deberá enviar un correo electrónico a la dirección <a href='mailto:legal@locatefit.es'>legal@locatefit.es</a>. Cuando reciba publicidad por correo electrónico, también podrá oponer desde dicho correo electrónico, pinchando en el enlace incluido en el mismo y siguiendo las instrucciones que le sean facilitadas.</p>

          <h3>11. Cambios a la Política de Privacidad</h3>

          <p>Nuestra Política de Privacidad podrá sufrir actualizaciones, debidas a cambios y necesidades legales, así como debidas a mejoras y cambios incluidos en la forma de ofrecer y prestar nuestros servicios y utilidades de la aplicación. Por ello, le recomendamos que visite y acceda a nuestra Política de Privacidad periódicamente, para poder tener acceso y conocer los últimos cambios que hayan podido ser incorporados. En caso de que dichos cambios guarden relación con el consentimiento prestado por el usuario, en tal caso le será enviada una notificación independiente y por separado para recabarlo nuevamente.</p>

          <p>Si durante la lectura le ha surgido alguna duda o cuestión sobre nuestra Política de Privacidad o quiere ejercitar algún derecho o acción relativa a sus datos personales, por favor póngase en contacto con nosotros en la siguiente dirección de correo electrónico <a href='mailto:legal@locatefit.es'>legal@locatefit.es</a>.</p>
        </div>

        <Footer />




      </div>



    )
  }
}


export default Privacidad;
