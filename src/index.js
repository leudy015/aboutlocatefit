import React from 'react';

import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router-dom';
import { BrowserRouter as Router} from 'react-router-dom';


import './index.css';
import App from './App';
import Quienes from "./container/quienessomos/quienessomos";
import Empleo from "./container/empleo/empleo";
import Profesionales from "./container/empleo/profesionales";
import Preguntas from "./container/ayuda/pregunta";
import Contacto from "./container/ayuda/contacto";
import Condiciones from "./container/legal/condiciones";
import Privacidad from "./container/legal/privacidad";
import Cookies from "./container/legal/cookies";
import NoEncontrado from "./container/404/page404";

import * as serviceWorker from './serviceWorker';

const routing = (
  <Router>
    <div>
    <Switch>
      <Route exact path="/" component={Quienes} />
      <Route  exact path="/empleo" component={Empleo} />
      <Route  exact path="/profesionales" component={Profesionales} />
      <Route  exact path="/preguntas-frecuentes" component={Preguntas} />
      <Route  exact path="/contacto" component={Contacto} />
      <Route  exact path="/condiciones-de-uso" component={Condiciones} />
      <Route  exact path="/privacidad" component={Privacidad} />
      <Route  exact path="/cookies" component={Cookies} />
      <Route component={NoEncontrado} />
      </Switch>
    </div>
  </Router>
)



ReactDOM.render(routing, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
