import React from 'react';
import { BrowserRouter as Router, Route, Switch,BrowserRouter } from "react-router-dom";



import App from './App';
import Quienes from "./container/quienessomos/quienessomos";
import Empleo from "./container/empleo/empleo";




const AppRoutes = () =>
<App>
  <switch>
  <Route exact path="/" component={Quienes} />
  <Route exact path="/empleo" component={Empleo} />
  </switch>
</App>;

export default AppRoutes;
