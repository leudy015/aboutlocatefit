import React, { Component } from "react";
import { Link } from "react-router-dom";
import './footer.css';
const mainLogo = require('./../../assets/img/logomainabout.png');
const country_flag = require('./../../assets/img/country_flag/country_flag.png');
const country_flag1 = require('./../../assets/img/country_flag/France.svg');
const country_flag2 = require('./../../assets/img/country_flag/ingles.png');
const country_flag3 = require('./../../assets/img/country_flag/Italy.svg');
const country_flag4 = require('./../../assets/img/country_flag/Portugal.svg');

class Footer extends Component {
  render() {
    return (
      <div>
        <section className="bottomFooter">
          <div className="container">
            <div className="row">
              <div className="col-md-3">
                <div className="mainLogo">
                  <img className="mainLogo" src={mainLogo} />

                </div>

                <label className="logoTitle">Los mejores profesionales,
                <br />tu eliges la fecha, hora y lugar.</label>
              </div>
              <div className="col-md-2">
                <label className="footerLabel">Trabaja con<br /> Nosotros</label>
                <ul className="footerLink">
                <li>
                  <a href="/">¿Quienes somos?</a>
                </li>
                  <li>
                    <a href="/empleo">Empleos</a>
                  </li>
                  <li>
                    <a href="/profesionales">Profesionales</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2">
                <label className="footerLabel">Atención al<br /> Cliente</label>
                <ul className="footerLink">
                  <li>
                    <a className="ayuda" href="/preguntas-frecuentes">Preguntas frecuentes</a>
                  </li>
                  <li>
                    <a className="ayuda1" href="/contacto">Contacto</a>
                  </li>
                  <li>
                    <a target="_blank" className="ayuda1" href="https://blog.locatefit.es">Blog</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-3">
                <label className="footerLabel">Términos<br />Legales</label>
                <ul className="footerLink">
                  <li>
                    <a href="/condiciones-de-uso">Condiciones de uso</a>
                  </li>
                  <li>
                    <a href="/privacidad">Políticas de privacidad</a>
                  </li>
                  <li>
                    <a href="/cookies">Cookies</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-21">
              <label className="footerLabel"></label>
              <div class="dropdown">

                    <button class="btn footerDropdown dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img class="flag" src={country_flag} height={15}/> Español
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                      <button  class="dropdown-item" type="button">
                      <img class="flag" src={country_flag2} height={15}/>Inglés
                      </button>

                      <button class="dropdown-item" type="button">
                      <img class="flag" src={country_flag1} height={15}/>Francés
                        </button>
                      <button class="dropdown-item" type="button">
                      <img class="flag" src={country_flag4} height={15}/>Portugés</button>
                      <button class="dropdown-item" type="button">
                      <img class="flag" src={country_flag3} height={15}/>Italiano</button>

                    </div>
                  </div>
              </div>
            </div>
          </div>
          <p className="logoTitle1">Copyright © 2019 Locatefit S.L. © de sus respectivos propietarios.</p>
        </section>

      </div>
    );
  }
}

export default Footer;
