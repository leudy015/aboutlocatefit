import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//Component
import './header.css';
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Headercontacto extends Component {
  render() {
    return (
      <section className="header1">
        <Example />

        <div className="logo">
          <a href="/"><img width="220px" src={mainLogo} /></a>
        </div>

        <div className="titulodelencabezado">
          <h1>¡Hablemos!</h1>
          <p>En caso de cualquier inquietud no dudes en contactarnos, estamos aquí para ayudarte.</p>
        </div>


        <div className="wave wave1"></div>
        <div className="wave wave2"></div>
        <div className="wave wave3"></div>
        <div className="wave wave4"></div>

      </section>



    )
  }
}


export default Headercontacto;
