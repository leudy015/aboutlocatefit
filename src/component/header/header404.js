import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//Component
import './header.css';
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Header404 extends Component{
  render(){
    return(
      <section className="header1">
      <Example />

      <div className="logo">
          <a href="/"><img  width="220px" src={mainLogo} /></a>
      </div>

      <div className="titulodelencabezado">
          <h1>Uhhh! Pagina no encotrada 404</h1>
          <p>Parece que lo que estás intentando burcar no se encuentra en nuestros directorios.</p>
      </div>


      <div className="wave wave1"></div>
            <div className="wave wave2"></div>
            <div className="wave wave3"></div>
            <div className="wave wave4"></div>
           
          </section>



    )
  }
}


export default Header404;
