import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//Component
import './header.css';
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Headerpregunta extends Component {
  render() {
    return (
      <section className="header1">
        <Example />

        <div className="logo">
          <a href="/"><img width="220px" src={mainLogo} /></a>
        </div>

        <div className="titulodelencabezado">
          <h1>Preguntas Frecuentes</h1>
          <p>Aquí tienes algunas de la preguntas más frecuentes y si éstas no te ayudan, no dudes en contactarnos.</p>
        </div>


        <div className="wave wave1"></div>
        <div className="wave wave2"></div>
        <div className="wave wave3"></div>
        <div className="wave wave4"></div>

      </section>



    )
  }
}


export default Headerpregunta;
