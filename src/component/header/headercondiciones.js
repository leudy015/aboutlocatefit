import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//Component
import './header.css';
import flecha from './../../assets/img/flecha.svg'
import header from './../../assets/img/header.svg'
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Headercondiciones extends Component{
  render(){
    return(
      <section className="header1">
      <Example />

      <div className="logo">
          <a href="/"><img  width="220px" src={mainLogo} /></a>
      </div>

      <div className="titulodelencabezado">
          <h1>Condiciones de uso</h1>
          <p>Aquí plasmanos todas nuestras condiciones de uso y compras de nuestra plataforma</p>
      </div>


      <div className="wave wave1"></div>
            <div className="wave wave2"></div>
            <div className="wave wave3"></div>
            <div className="wave wave4"></div>
           
          </section>



    )
  }
}


export default Headercondiciones;
