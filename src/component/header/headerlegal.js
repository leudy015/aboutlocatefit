import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";



//Component
import './header.css';
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Headerlegal extends Component {
  render() {
    return (
      <section className="header1">
        <Example />

        <div className="logo">
          <a href="/"><img width="220px" src={mainLogo} /></a>
        </div>

        <div className="titulodelencabezado">
          <h1>Política de privacidad</h1>
          <p>Tu privacidad nos interesa, por ello la tratamos con la mayor seguridad</p>
        </div>


        <div className="wave wave1"></div>
        <div className="wave wave2"></div>
        <div className="wave wave3"></div>
        <div className="wave wave4"></div>

      </section>



    )
  }
}


export default Headerlegal;
