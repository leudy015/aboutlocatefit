import React, { Component } from 'react';
import './header.css';
import Example from "./../../component/header/header2.js";
import mainLogo from './../../assets/img/logomainabout.png'

class Header extends Component {
  render() {
    return (
          <section className="header">
              <Example />
            <div className="logo">
                <a href="/"><img width="220px" src={mainLogo} /></a>
            </div>
                <div className="headercontenedor">
                  <p>¿Qué es Locatefit?</p>
                  <h1>Somos la mejor plataforma <br />de profesionales.</h1>
                  <p>Empieza a trabajar por tu cuenta, se libre.</p>
                  <a href="https://locatefit.es/become-profesional">Ofrecer mis servicios</a>
                </div>
            <div className="wave wave1"></div>
            <div className="wave wave2"></div>
            <div className="wave wave3"></div>
            <div className="wave wave4"></div>
          </section>
    )
  }

}

export default Header;
